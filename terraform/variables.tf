variable "gcp_project" {
  type        = string
  description = "The name of the Google Cloud Project where the cluster is to be provisioned"
}

variable "gcp_zone" {
  type        = string
  default     = "europe-west4-a"
  description = "The name of the Google zone where the cluster nodes are to be provisioned"
}


variable "cluster_name" {
  type        = string
  default     = "gitlab-terraform-gke"
  description = "The name of the cluster to appear on the Google Cloud Console"
}

variable "cluster_description" {
  type        = string
  default     = "This cluster is managed by GitLab https://gitlab.com/Ivorlun/microservices-demo"
  description = "A description for the cluster. We recommend adding the $CI_PROJECT_URL variable to describe where the cluster is configured."
}


variable "machine_type" {
  type        = string
  default     = "n2-standard-2"
  description = "The name of the machine type to use for the cluster nodes"
}

variable "disk_size_gb" {
  description = "The default disk size the nodes are given 100gb by default. Don't set it too low though as disk I/O is also tied to disk size."
  type        = number
  default     = 25
}

variable "enable_cluster_autoscaling" {
  description = "Enable node auto-provisioning."
  type        = bool
  default     = false
}

variable "initial_node_count" {
  default     = 4
  description = "The initial number of cluster nodes"
}

variable "min_node_count" {
  default     = 1
  description = "The minimum number of cluster nodes"
}

variable "max_node_count" {
  default     = 4
  description = "The maximum number of cluster nodes"
}
