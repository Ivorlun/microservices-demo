resource "google_container_cluster" "primary" {
  name                     = var.cluster_name
  location                 = var.gcp_zone
  remove_default_node_pool = true
  initial_node_count       = 1
  min_master_version       = "1.26"
  description              = var.cluster_description

  release_channel {
    channel = "RAPID"
  }
  node_config {
    preemptible  = true
    disk_size_gb = var.disk_size_gb
  }
  cluster_autoscaling {
    enabled = var.enable_cluster_autoscaling
    auto_provisioning_defaults {
      disk_size = var.disk_size_gb
    }
  }
  lifecycle {
    ignore_changes = [
      initial_node_count
    ]
  }
}

resource "google_container_node_pool" "primary_nodes" {
  name               = "${var.cluster_name}-node-pool"
  cluster            = google_container_cluster.primary.name
  location           = var.gcp_zone
  initial_node_count = var.initial_node_count
  autoscaling {
    min_node_count = var.min_node_count
    max_node_count = var.max_node_count
  }
  lifecycle {
    ignore_changes = [
      initial_node_count
    ]
  }

  node_config {
    preemptible  = true
    machine_type = var.machine_type
    disk_size_gb = var.disk_size_gb

    metadata = {
      disable-legacy-endpoints = "true"
    }

    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]
  }
}
