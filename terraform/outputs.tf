output "env-dynamic-url" {
  value = "https://${google_container_cluster.primary.endpoint}"
}

# output "static-ip-address" {
#   value = "${google_compute_address.default.address}"
# }
